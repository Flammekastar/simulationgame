﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;
using UnityEngine.SceneManagement;
using UnityEngine.Networking;

public class InputControl : NetworkBehaviour
{

    //public object GameControl { get; private set; }

    public GameObject g;

    void Start()
    {
        var input = gameObject.GetComponent<InputField>();      
        var se = new InputField.SubmitEvent();
        se.AddListener(CmdSubmitName);
        input.onEndEdit = se;

        //or simply use the line below, 
        //input.onEndEdit.AddListener(SubmitName);  // This also works
    }

    [Command]
    private void CmdSubmitName(string arg0)
    {
        GlobalControl gc = g.GetComponent<GlobalControl>();
        Debug.Log(arg0);
        gc.gamePin = Int32.Parse(arg0);
        SceneManager.LoadScene(1);

    }
}