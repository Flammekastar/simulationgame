﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using UnityEngine.UI;
using System;

public class Player : NetworkBehaviour {

    private GameObject g;
    private int number;
    public int playNum;
    public InputField input;
    private Button okButton;
    public string playerType;
    private Text txt;
    private GlobalControl gc;
    private Player thisPlayer;

    // Use this for initialization
    void Start () {
            thisPlayer = this;
            g = GameObject.Find("SendButton");
            okButton = g.GetComponent<Button>();
            //okButton.onClick.AddListener(() => buttonClicked());
            g = GameObject.Find("InputField");
            input = g.GetComponent<InputField>();
            g = GameObject.Find("GameMaster");
            gc = g.GetComponent<GlobalControl>();

            switch (gc.amountOfPlayers)
            {
                case 0:
                    playerType = "Factory";
                    txt = GameObject.Find("factorytxt").GetComponent<Text>();
                    playNum = 0;
                    txt.text = "Factory: Connected";
                    break;
                case 1:
                    playerType = "Distributor";
                    txt = GameObject.Find("distributortxt").GetComponent<Text>();
                    playNum = 1;
                    txt.text = "Distributor: Connected";
                    break;
                case 2:
                    playerType = "Wholesaler";
                    txt = GameObject.Find("wholetxt").GetComponent<Text>();
                    playNum = 2;
                    txt.text = "Wholesaler: Connected";
                    break;
                case 3:
                    playerType = "Retailer";
                    txt = GameObject.Find("retailtxt").GetComponent<Text>();
                    playNum = 3;
                    txt.text = "Retail: Connected";
                    break;
                default:
                    playerType = "can't happen!";
                    break;
            }
            gc.amountOfPlayers++;
            buttonToggler();
	}
	
	// Update is called once per frame
	void Update () {
            buttonToggler();
    }
    private void buttonToggler()
    {
        if (gc.playersTurn == thisPlayer.playNum)
        {
            //okButton.GetComponent<Button>().interactable = true;
        }
        
           // okButton.GetComponent<Button>().interactable = false;
    }
    [Command]
    private void CmdturnChanger()
    {
        gc.playersTurn++;
        if (gc.playersTurn >= gc.amountOfPlayers)
            gc.playersTurn = 0;
    }

    [Command]
    private void CmdbuttonClicked()
    {
        Debug.Log("ndksofksdf0");
        txt = GameObject.Find("TheInputText").GetComponent<Text>();
        number = Int32.Parse(txt.text);
        CmdTest(number);
        CmdturnChanger();
    }

    [Command]
    private void CmdTest(int arg0)
    {
        Debug.Log(arg0);
        gc.theNumber = arg0;
    }
}
