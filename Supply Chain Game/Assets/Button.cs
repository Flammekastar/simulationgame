﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;
using System.Collections;
using System;

public class Button : NetworkBehaviour {
    private Text txt;
    private int number;

    [Command]
    private void CmdbuttonClicked()
    {
        Debug.Log("ndksofksdf0");
        txt = GameObject.Find("TheInputText").GetComponent<Text>();
        number = Int32.Parse(txt.text);
        CmdTest(number);
        CmdturnChanger();
    }
    [Command]
    private void CmdTest(int arg0)
    {
        GameObject g = GameObject.Find("GameMaster");
        Debug.Log(arg0);
        GlobalControl gc = g.GetComponent<GlobalControl>();
        gc.theNumber = arg0;
    }
    [Command]
    private void CmdturnChanger()
    {
        GameObject g = GameObject.Find("GameMaster");
        GlobalControl gc = g.GetComponent<GlobalControl>();
        gc.playersTurn++;
        if (gc.playersTurn >= gc.amountOfPlayers)
            gc.playersTurn = 0;
    }
}
