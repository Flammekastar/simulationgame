﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class GlobalControl : NetworkBehaviour
{
    [SyncVar]
    public int gamePin;
    [SyncVar]
    public int theNumber;
    [SyncVar]
    public int amountOfPlayers;
    [SyncVar]
    public int playersTurn;

    public static GlobalControl Instance;

  

    void Awake()
    {
        if (Instance == null)
        {
            DontDestroyOnLoad(gameObject);
            Instance = this;
        }
        else if (Instance != this)
        {
            Destroy(gameObject);
        }
    }
}