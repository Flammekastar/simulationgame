﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;
using System.Collections;
using System;

public class InputTest : NetworkBehaviour
{

    public object GameControl { get; private set; }

    void Start()
    {
        var input = gameObject.GetComponent<InputField>();
        var se = new InputField.SubmitEvent();
        se.AddListener(SubmitName);
        input.onEndEdit = se;

        //or simply use the line below, 
        //input.onEndEdit.AddListener(SubmitName);  // This also works
    }

    private void SubmitName(string arg0)
    {
        GameObject g = GameObject.Find("GameMaster");
        GlobalControl gc = g.GetComponent<GlobalControl>();
        Debug.Log(arg0);
        gc.theNumber = Int32.Parse(arg0);
    }
}